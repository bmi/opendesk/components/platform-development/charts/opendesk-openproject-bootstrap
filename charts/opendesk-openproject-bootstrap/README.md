<!--
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
-->
# opendesk-openproject-bootstrap

This helm chart contains a bootstrap job for configuration of OpenProject in context of openDesk

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-openproject-bootstrap https://gitlab.opencode.de/api/v4/projects/2284/packages/helm/stable
helm install my-release --version 2.2.0 opendesk-openproject-bootstrap/opendesk-openproject-bootstrap
```

### Install via OCI Registry

```console
helm repo add opendesk-openproject-bootstrap oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap
helm install my-release --version 2.2.0 opendesk-openproject-bootstrap/opendesk-openproject-bootstrap
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry.opencode.de/bmi/opendesk/components/external/charts/bitnami-charts | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| additionalAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| additionalLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| cleanup.deletePodsOnSuccess | bool | `false` | Keep Pods/Job logs after successful run. |
| cleanup.deletePodsOnSuccessTimeout | int | `1800` | Keep Pods/Job logs for following time. |
| cleanup.keepPVCOnDelete | bool | `false` | Keep persistence on delete of this release. |
| config.debug.enabled | bool | `false` | Enables debug output (set -x / set -v) within the shell scripts. |
| config.debug.pauseBeforeScriptStart | int | `0` | Pauses before executing the integration script. |
| config.nextcloud.admin.password.secret.key | string | `""` | Key in secret containing the admin password |
| config.nextcloud.admin.password.secret.name | string | `""` | Secret name containing the admin password |
| config.nextcloud.admin.password.value | string | `""` | The admin's password as plain value. |
| config.nextcloud.admin.username.secret.key | string | `""` | Key in secret containing username |
| config.nextcloud.admin.username.secret.name | string | `""` | Secret name containing username |
| config.nextcloud.admin.username.value | string | `"nextcloud"` | The admin's username as plain value. |
| config.nextcloud.setupProjectFolder | bool | `true` | Project folders are an option for the OpenProject-Nextcloud integration. This attribute enables or disables the option. |
| config.openproject.admin.password.secret.key | string | `""` | Key in secret containing the admin password |
| config.openproject.admin.password.secret.name | string | `""` | Secret name containing the admin password |
| config.openproject.admin.password.value | string | `""` | The admin's password as plain value. |
| config.openproject.admin.username.secret.key | string | `""` | Key in secret containing username |
| config.openproject.admin.username.secret.name | string | `""` | Secret name containing username |
| config.openproject.admin.username.value | string | `"openproject"` | The admin's username as plain value. |
| config.openproject.fileshareName | string | `"openDesk Nextcloud"` | The name of the fileshare as shown to the user in the "Files" tab of OpenProject. Do not use single quotes in the name! |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Security capabilities for container. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsGroup | int | `1000` | Process group id. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as a user. |
| containerSecurityContext.runAsUser | int | `1000` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraVolumeMounts | list | `[]` | Optionally specify an extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify an extra list of additional volumes. |
| global.domain | string | `"example.org"` | The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component. |
| global.hosts.nextcloud | string | `"fs"` | Subdomain for Keycloak, results in "https://{{ keycloak }}.{{ domain }}". |
| global.hosts.openproject | string | `"project"` |  |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"registry.opencode.de"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"bmi/opendesk/components/platform-development/images/opendesk-openproject-bootstrap"` | Container repository string. |
| image.tag | string | `"1.1.3"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| podAnnotations | object | `{}` | Additional custom annotations to add to pods. |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `1000` | If specified, all processes of the container are also part of the supplementary group. |
| podSecurityContext.fsGroupChangePolicy | string | `"Always"` | Change ownership and permission of the volume before being exposed inside a Pod. |
| resources.limits.memory | string | `"1Gi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"500m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"256Mi"` | The amount of RAM which has to be available on the scheduled node. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

# License

This project uses the following license: Apache-2.0

# Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
