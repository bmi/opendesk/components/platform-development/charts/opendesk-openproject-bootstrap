# [2.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/compare/v2.1.1...v2.2.0) (2025-03-04)


### Features

* Adds support for external secrets ([e1dab42](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/commit/e1dab42766424e903077ad9bbb74fa0c6b3716cd))

## [2.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/compare/v2.1.0...v2.1.1) (2024-10-08)


### Bug Fixes

* **opendesk-openproject-bootstrap:** Add argocd annotation ([4a7d855](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/commit/4a7d8550b0a955082284b7d8ba000b05fad0a70e))

# [2.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/compare/v2.0.0...v2.1.0) (2024-10-08)


### Features

* **opendesk-openproject-bootstrap:** Add additionalAnnotations,additionalLabels,podAnnotations ([da57d75](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/commit/da57d7571066ad8cbe33864348c5b82353726507))

# [2.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/compare/v1.4.0...v2.0.0) (2024-07-07)


### Bug Fixes

* **opendesk-openproject-bootstrap:** Use normal job without hook ([ba5fca8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/commit/ba5fca87dfa7a4a4341f7beb7d4635964b9bfad4))


### BREAKING CHANGES

* **opendesk-openproject-bootstrap:** Remove helm hook annotation

# [1.4.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/compare/v1.3.0...v1.4.0) (2024-06-25)


### Features

* **opendesk-openproject-bootstrap:** Add extraVolumes and extraVolumeMounts option ([ca9c496](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/commit/ca9c4964ed9468f4b26b36a7fb7fee1d731ccaea))

# [1.3.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/compare/v1.2.4...v1.3.0) (2024-01-21)


### Features

* **opendesk-openproject-bootstrap:** Fix imagepullpolicy and add missing service-account ([7ed124f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/commit/7ed124fc7bbb73a486d24d21cf92bb15df897df6))

## [1.2.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/compare/v1.2.3...v1.2.4) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([de5f450](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/commit/de5f450d5c1903537aa40858ef859fa11e351d6e))

## [1.2.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/compare/v1.2.2...v1.2.3) (2023-12-20)


### Bug Fixes

* **ci:** Add .common:tags: [] ([022a3e5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap/commit/022a3e544fdcf233dd8cb3c7f90cce184afbc482))

## [1.2.2](https://gitlab.opencode.de/bmi/opendesk/components/charts/opendesk-openproject-bootstrap/compare/v1.2.1...v1.2.2) (2023-12-19)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([edfb3da](https://gitlab.opencode.de/bmi/opendesk/components/charts/opendesk-openproject-bootstrap/commit/edfb3daf2aedadeb4d2f777a6f8763b7a7434efe))

## [1.2.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-openproject-bootstrap/compare/v1.2.0...v1.2.1) (2023-11-11)


### Bug Fixes

* **chart:** Proper templating of resources ([dd45b14](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-openproject-bootstrap/commit/dd45b140feb99bae2f3d568fec17ef42fe2b28c5))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-openproject-bootstrap/compare/v1.1.0...v1.2.0) (2023-11-11)


### Features

* **chart:** Add SecurityContexts ([160f369](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-openproject-bootstrap/commit/160f369afc6bea15865e8da8d2751213d4b045c7))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-openproject-bootstrap/compare/v1.0.0...v1.1.0) (2023-11-10)


### Features

* **chart:** First functional version ([f441efc](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-openproject-bootstrap/commit/f441efc170b9edc25440fe5bdebb4048a8ba300c))

# 1.0.0 (2023-11-07)


### Bug Fixes

* Initial Commit ([85cfcf8](https://gitlab.souvap-univention.de/souvap/tooling/charts/opendesk-openproject-bootstrap/commit/85cfcf88bdd8dc7ae19453cf43d7df8deed75f30))
